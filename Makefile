all: target/riskexplore-1.0.jar
	java -cp target/riskexplore-1.0.jar uk.ac.open.riskexplore.Search

target/riskexplore-1.0.jar: pom.xml
	mvn install
