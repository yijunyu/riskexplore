package uk.ac.open.riskexplore;
import org.jgrapht.graph.DefaultWeightedEdge;

public class Edge extends DefaultWeightedEdge implements Comparable<Object> {
	private static final long serialVersionUID = 5630928910278193462L;
	public String name;
	public String weight; // probability
	
	public Edge() {	}

	@Override
	public String toString() {
		if (Search.displayMode == Search.DisplayMode.HTML)
			return name + " " + Search.yacas.Evaluate(weight);
		return name + ": " + ((Vertex) this.getSource()).stateName + "->" 
				     + ((Vertex) this.getTarget()).stateName
				+ " ("+ Search.yacas.Evaluate(weight) + ")";	
	}

	public int compareTo(Object o) {
		return toString().compareTo(((Edge)o).toString());
	}
	
}
