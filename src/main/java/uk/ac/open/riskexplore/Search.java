package uk.ac.open.riskexplore;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import org.jgrapht.EdgeFactory;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.ClassBasedEdgeFactory;
import org.jgrapht.graph.DefaultDirectedWeightedGraph;

import net.sf.yacas.YacasInterpreter;
import no.uib.cipr.matrix.DenseLU;
import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.DenseVector;
import no.uib.cipr.matrix.Matrices;
import no.uib.cipr.matrix.Matrix;
import no.uib.cipr.matrix.MatrixSingularException;
import no.uib.cipr.matrix.PermutationMatrix;
import no.uib.cipr.matrix.UnitLowerTriangDenseMatrix;
import no.uib.cipr.matrix.UpperTriangDenseMatrix;

/**
 * Algorithms to compute risks of an exploration machine
 * 
 * @author Yijun Yu
 */
public class Search {

	public static DefaultDirectedWeightedGraph<Vertex, Edge> emInitial; // initial
	public static DefaultDirectedWeightedGraph<Vertex, Edge> emPreprocessed; // preprocessed
	public static DefaultDirectedWeightedGraph<Vertex, Edge> emNormalised; // normalised

	public static Vertex addVertex(DefaultDirectedWeightedGraph<Vertex, Edge> graph, String state, String impact) {
		Vertex v = new Vertex(state, impact);
		graph.addVertex(v);
		return v;
	}

	public static Edge addEdge(DefaultDirectedWeightedGraph<Vertex, Edge> graph, Vertex v1, Vertex v2,
			String transition, String probability) {
		if (!graph.containsVertex(v1) || !graph.containsVertex(v2)) {
			System.err.println("not found the vertex: " + v1 + " or " + v2);
			System.exit(1);
		}
		Vertex vertex1 = findVertex(graph, v1.stateName);
		Vertex vertex2 = findVertex(graph, v2.stateName);
		if (!graph.containsVertex(vertex1) || !graph.containsVertex(vertex2)) {
			System.err.println("not found the vertex: " + vertex1 + " or " + vertex2);
			System.exit(1);
		}
		Edge e = graph.addEdge(vertex1, vertex2);
		if (e != null) {
			e.name = transition;
			e.weight = probability;
		} else {
			System.err.println("not found the edge: " + vertex1 + " -> " + vertex2);
			System.exit(1);
		}
		return e;
	}

	public static Vertex findVertex(DefaultDirectedWeightedGraph<Vertex, Edge> em, String stateName) {
		for (Vertex v : em.vertexSet()) {
			if (stateName.equals(v.stateName)) {
				return v;
			}
		}
		return null;
	}

	public static Edge findEdge(DefaultDirectedWeightedGraph<Vertex, Edge> graph, String name, String name1,
			String name2) {
		for (Edge e : graph.edgeSet()) {
			if (!e.name.equals(name))
				continue;
			if (!graph.getEdgeSource(e).stateName.equals(name1))
				continue;
			if (!graph.getEdgeTarget(e).stateName.equals(name2))
				continue;
			return e;
		}
		return null;
	}

	public static void printGraph(DefaultDirectedWeightedGraph<Vertex, Edge> graph) {
		System.out.println("nodes {");
		for (Vertex v : graph.vertexSet()) {
			System.out.println(v);
		}
		System.out.println("}");
		System.out.println("edges {");
		for (Edge e : graph.edgeSet()) {
			System.out.println(e);
		}
		System.out.println("}");
	}

	public static boolean isUniform(DefaultDirectedWeightedGraph<Vertex, Edge> graph) {
		boolean uniform = true;
		for (Vertex v : graph.vertexSet()) {
			if (graph.outDegreeOf(v) > 0) {
				double sum = 0;
				for (Edge e : graph.outgoingEdgesOf(v)) {
					sum += graph.getEdgeWeight(e);
				}
				if (Math.abs(sum - 1.0) > 0.00001f) {
					System.err.println("The sum of likelihood of outgoing transitions of " + v + " is not unit:" + sum);
					for (Edge e : graph.outgoingEdgesOf(v)) {
						System.err.println(e);
					}

				}
			}
		}
		return uniform;
	}

	enum DisplayMode {
		HTML, PLAIN
	};

	public static DisplayMode displayMode;

	/**
	 * 
	 * Detecting cycles.
	 * 
	 * @param graph
	 */
	public static boolean hasCycle(DefaultDirectedWeightedGraph<Vertex, Edge> graph) {
		return new CycleDetector<Vertex, Edge>(graph).detectCycles();
	}

	static boolean debug = false;
	public static String precision = "5";

	public static void printMatrix(String prompt, Matrix m, List<Vertex> traversed_nodes) {
		if (!debug) return;
		DecimalFormat df = new DecimalFormat("#.##");
		System.out.print(prompt + " =\t");
		for (int j = 0; j < m.numColumns(); j++) {
			System.out.print("\t" + j);
		}
		System.out.println();
		System.out.print("\t");
		for (int j = 0; j < m.numColumns(); j++) {
			System.out.print("\t" + "---");
		}
		System.out.println();
		for (int i = 0; i < m.numRows(); i++) {
			System.out.print("\t" + i + " |");
			for (int j = 0; j < m.numColumns(); j++) {
				System.out.print("\t" + df.format(m.get(i, j)));
			}
			if (traversed_nodes.size() >= m.numRows())
				System.out.print("\t|" + traversed_nodes.get(i).stateName);
			System.out.println();
		}
	}
	
	private static String toMatrix(String M[][]) {
		String s = "{";
		int n = M.length;
		for (int i = 0; i < n; i++) {
			if (i > 0)
				s += ",";
			s += "{";
			for (int j = 0; j < n; j++) {
				if (j > 0)
					s += ",";
				s += M[i][j];
			}
			s += "}";
		}
		s += "}";
		return s;
	}

	public static void printMatrix(String prompt, String M, List<Vertex> traversed_nodes) {
		if (!debug) return;
		int n = traversed_nodes.size();
		String type = yacas.Evaluate("Type(" + M + "[1])");
		int m = 1;
		if (type.equals("\"List\""))
			m = Integer.parseInt(yacas.Evaluate("Length(" + M + "[1])"));
		System.out.print(prompt + " =\t");
		for (int j = 0; j < m; j++) {
			System.out.print("\t" + j);
		}
		System.out.println();
		System.out.print("\t");
		for (int j = 0; j < m; j++) {
			System.out.print("\t" + "---");
		}
		System.out.println();
		for (int i = 0; i < n; i++) {
			System.out.print("\t" + i + " |");
			for (int j = 0; j < m; j++) {
				if (m == 1) {
					System.out.print("\t" + yacas.Evaluate(M + "[" + (i + 1) + "]"));
				} else
					System.out.print("\t" + yacas.Evaluate(M + "[" + (i + 1) + "]" + "[" + (j + 1) + "]"));
			}
			if (traversed_nodes.size() >= n)
				System.out.print("\t|" + traversed_nodes.get(i).stateName);
			System.out.println();
		}
	}
	private static String transition_matrix = "";
	private static void symbolicSolve(Model model, String sP[][],
			List<Vertex> traversed_nodes) throws FileNotFoundException {
		DefaultDirectedWeightedGraph<Vertex, Edge> graph = model.graph;
		int n = sP.length;
		String impact = "{";
		for (int i = 0; i < n; i++) {
			if (i>0)
				impact += ",";
			impact += traversed_nodes.get(i).impact;
		}
		impact += "}";
		String P = toMatrix(sP);
		if (debug)
			System.out.println("NormaliseMDP(" + P + "," + impact + ")");
		String sXPC = yacas.Evaluate("NormaliseMDP(" + P + "," + impact + ")");
		String sr = yacas.Evaluate(sXPC + "[1]");
		String sp = yacas.Evaluate(sXPC + "[2]");
		String sp2 = yacas.Evaluate(sXPC + "[3]");
		String sP1 = yacas.Evaluate(sXPC + "[4]");
		String sc2 = yacas.Evaluate(sXPC + "[5]");
		String sP2 = yacas.Evaluate(sXPC + "[6]");
		String i2 = yacas.Evaluate(sXPC + "[7]");		
		String diag = yacas.Evaluate(sXPC + "[8]");
		transition_matrix = sP2;
//		transition_matrix = P;
		print_determinants(model.filename, diag);
//		System.out.println(sXPC);
		printMatrix("r", sr, traversed_nodes);
//		printMatrix("i", impact, traversed_nodes);
//		printMatrix("p", sp, traversed_nodes);
//		printMatrix("p2", sp2, traversed_nodes);
//		printMatrix("i2", i2, traversed_nodes);
		System.out.println(diag);
		// computing the risks
		List<String> risks = new ArrayList<String>();
		if (traversed_nodes.size() >= n)
			for (int i = 0; i < n; i++) {
				Vertex s = traversed_nodes.get(i);
				String expr = sp + "[" + (i+1) + "]" + "*" + s.impact;
				String risk = yacas.Evaluate("N(" + expr + ")");
				risks.add(risk);
			}		
		print_risk(model.filename, risks);
		printMatrix("P'", sP2, traversed_nodes);
		printMatrix("c'", sc2, traversed_nodes);	
		
		emPreprocessed = new DefaultDirectedWeightedGraph<Vertex, Edge>(edgeFactory);
		if (traversed_nodes.size() >= n) {
			for (int i = 0; i < n; i++) {
				Vertex s = traversed_nodes.get(i);
				Vertex s_p = addVertex(emPreprocessed, s.stateName, s.impact);
				s_p.risk = risks.get(i);
			}
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < i; j++) {
					String condition = yacas.Evaluate(sP1 + "[" + (i+1) + "][" + (j+1) + "]=0");
					if (!condition.equals("True")) {
						String iName = traversed_nodes.get(i).stateName;
						String jName = traversed_nodes.get(j).stateName;
						String e_name = "";
						for (Edge e : graph.edgeSet()) {
							if (graph.getEdgeTarget(e).stateName.equals(iName)
									&& graph.getEdgeSource(e).stateName.equals(jName)) {
								e_name = e.name;
								break;
							}
						}
						Vertex vi_p = findVertex(emPreprocessed, iName);
						Vertex vj_p = findVertex(emPreprocessed, jName);
						String evaluate = yacas.Evaluate("N(" +sP1 + "[" + (i+1) + "][" + (j+1) + "]"+","+precision+")");
						addEdge(emPreprocessed, vj_p, vi_p, e_name, "" + evaluate);						
					}
				}
			}
		}
		emNormalised = new DefaultDirectedWeightedGraph<Vertex, Edge>(edgeFactory);
		if (traversed_nodes.size() >= n) {
			for (int i = 0; i < n; i++) {
				Vertex s = traversed_nodes.get(i);
				Vertex s_p = addVertex(emNormalised, s.stateName, yacas.Evaluate("N(" + sp2 + "[" + (i+1) + "]" + ","+precision+")"));
				
				String s_p1 = yacas.Evaluate(sp +"[" + (i+1) + "]");
				String s_p2 = yacas.Evaluate(sp2 +"[" + (i+1) + "]");
				String s_i1 = yacas.Evaluate(impact +"[" + (i+1) + "]");
				String expr = "N(N(" + s_i1 + ")*N(" + s_p1 + ")/N(" + s_p2+"))";
				// System.out.println(expr);
				s_p.impact = yacas.Evaluate(expr);
				// System.out.println(s_p.impact);
				s_p.risk = risks.get(i);
			}
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < i; j++) {
					String condition = yacas.Evaluate(sP2 + "[" + (i+1) + "][" + (j+1) + "]=0");
					if (!condition.equals("True")) {
						String iName = traversed_nodes.get(i).stateName;
						String jName = traversed_nodes.get(j).stateName;
						String e_name = "";
						for (Edge e : graph.edgeSet()) {
							if (graph.getEdgeTarget(e).stateName.equals(iName)
									&& graph.getEdgeSource(e).stateName.equals(jName)) {
								e_name = e.name;
								break;
							}
						}
						Vertex vi_p = findVertex(emNormalised, iName);
						Vertex vj_p = findVertex(emNormalised, jName);
						String evaluate = yacas.Evaluate("N(" + sP2 + "[" + (i+1) + "][" + (j+1) + "]" + ","+precision+")");
						addEdge(emNormalised, vj_p, vi_p, e_name, "" + evaluate);						
					}
				}
			}
		}
	}

	private static void print_risk(String filename, List<String> risks) throws FileNotFoundException {
		String risk = "0";
		for (String r: risks)
			risk = risk + " + (" + r + ")";
		File riskFile = new File(filename + ".risks");
		PrintStream ps = new PrintStream(riskFile);
		System.out.println("The total risks is " + risk);
		ps.println(yacas.Evaluate("N(" + risk + ","+precision+")"));
		ps.close();
	}	

	private static void print_determinants(String filename, String diag) throws FileNotFoundException {		
		File detFile = new File(filename + ".det");
		PrintStream ps = new PrintStream(detFile);
		ps.println(diag);
		ps.close();
	}
	
	private static List<Vertex> the_traversed_nodes = null;
	
	/**
	 * Computing the risks using an algebraic approach.
	 * 
	 * @param model
	 * @param numeric - consider numeric algorithm
	 * @return
	 */
	public static void solveRisks(Model model, boolean numeric) {
		try {
			DefaultDirectedWeightedGraph<Vertex, Edge> graph = model.graph; 
			// Here n is the number of states, P is the coefficient matrix
			int n = graph.vertexSet().size();
			Matrix P = new DenseMatrix(n, n);
			P.zero(); // set it to zero
			List<Vertex> traversed_nodes = new ArrayList<Vertex>();
			Map<Vertex, Integer> lookup = new HashMap<Vertex, Integer>();
			int i = 0;
			for (Vertex v: graph.vertexSet()) {
				traversed_nodes.add(v);
				lookup.put(v, new Integer(i++));
			}		
			the_traversed_nodes = traversed_nodes;
			String sP[][] = new String[n][n];
			for (i = 0; i < n; i++)
				for (int j = 0; j < n; j++)
					sP[i][j] = "0";
			boolean symbolic = false;
			Set<Edge> toRemove = new HashSet<Edge>();
			for (i = 0; i < n; i++) {
				if (traversed_nodes.size() >= n) {
					Vertex v = traversed_nodes.get(i);
					for (Edge e : graph.incomingEdgesOf(v)) {
						Vertex vi = graph.getEdgeSource(e);
						int j = lookup.get(vi).intValue();
						try {
							if (i!=j || Double.parseDouble(e.weight)!=1.0)
								P.set(i, j, Double.parseDouble(e.weight));
							else
								toRemove.add(e);
						} catch (NumberFormatException ex) {
							symbolic = true;
						}
						if (i != j || yacas.Evaluate(e.weight+"=1").equals("False"))
							sP[i][j] = e.weight;
						else
							toRemove.add(e);
					}
				}
			}
			for (Edge e: toRemove) {
				Vertex vi = graph.getEdgeSource(e);
				Vertex vj = graph.getEdgeTarget(e);
				graph.removeEdge(vi, vj);
			}
			symbolic = true;
			if (!symbolic || numeric)	solve(model, P, traversed_nodes);
			else symbolicSolve(model, sP, traversed_nodes);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private static void solve(Model model, Matrix P,
			List<Vertex> traversed_nodes) throws FileNotFoundException {
		DefaultDirectedWeightedGraph<Vertex, Edge> graph = model.graph;
		printGraph(graph);
		int n = P.numRows();
//		print_impacts(traversed_nodes);
		printMatrix("P", P, traversed_nodes);
		DenseMatrix I = Matrices.identity(n);
		DenseMatrix J = new DenseMatrix(n, n);
		J.zero();
		for (int i = 0; i < n; i++)
			J.set(i, n - i - 1, 1);
		Matrix IP = I.copy().add(-1, P);
		printMatrix("I - P", IP, traversed_nodes);
		Matrix IP1 = J.copy().mult(IP, IP.copy()).mult(J.copy(), J.copy());
		printMatrix("IP1=J(I - P)J", IP1, traversed_nodes);
		DenseVector c = new DenseVector(n);
		c.set(0, 1);
		for (int i = 1; i < n; i++)
			c.set(i, 0);
		DenseMatrix C = new DenseMatrix(c);
		Matrix p;
		try {
			p = IP.solve(C, C.copy());
		} catch (MatrixSingularException e) {
			System.err.println("The matrix I-P is singular");
			return;
		}
		DenseLU LU = DenseLU.factorize(IP1);
		if (LU.isSingular()) {
			System.err.println("The matrix is singular!");
			return;
		}
		PermutationMatrix M1 = LU.getP();
		printMatrix("M", M1, traversed_nodes);
		UnitLowerTriangDenseMatrix L = LU.getL();
		printMatrix("L", L, traversed_nodes);
		UpperTriangDenseMatrix U = LU.getU();
		printMatrix("U", U, traversed_nodes);
		Matrix M = J.copy().mult(M1, IP.copy()).mult(J.copy(), J.copy());
		Matrix U2 = J.copy().mult(L, IP.copy()).mult(J.copy(), J.copy());
		Matrix L2 = J.copy().mult(U, IP.copy()).mult(J.copy(), J.copy());
		// LU => LDU
		Matrix D = Matrices.identity(n);
		for (int i = 0; i < n; i++) {
			D.set(i, i, U2.get(i, i) * L2.get(i, i));
			for (int j = 0; j < n; j++) {
				U2.set(i, j, U2.get(i, j) / U2.get(i, i));
				L2.set(j, i, L2.get(j, i) / L2.get(i, i));
			}
		}
		printMatrix("D", D, traversed_nodes);
		Matrix D1 = D.solve(I, I.copy());
		Matrix U1 = U2.solve(I, I.copy());
		Matrix MI = M.solve(I, I.copy());
		printMatrix("L'", L2, traversed_nodes);
		printMatrix("D^-1", D1, traversed_nodes);
		printMatrix("U'^-1", U1, traversed_nodes);
		printMatrix("M'^-1", MI, traversed_nodes);
		Matrix DU = U2.copy().mult(D, D.copy());
		Matrix DU1 = DU.solve(I, I.copy());
		Matrix P1 = I.copy().add(-1, L2);
		printMatrix("P'=I - L'", P1, traversed_nodes);
		Matrix C1 = DU1.mult(MI, MI.copy()).mult(C, C.copy());
		printMatrix("C1", C1, traversed_nodes);
		ArrayList<Vertex> nodes = new ArrayList<Vertex>();
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				if (M.get(i, j) == 1) {
					if (traversed_nodes.size() >= n)
						nodes.add(traversed_nodes.get(j));
				}
		printMatrix("I - L'", P1, nodes);
		printMatrix("p", p, nodes);

		// recording the likelihood
		// System.out.println("Computing risks by multiplying with impact");
		// computing the risks
		// print_impacts(traversed_nodes);
		List<String> risks = new ArrayList<String>();
		if (traversed_nodes.size() >= n)
			for (int i = 0; i < n; i++) {
				Vertex s = traversed_nodes.get(i);
				String risk = yacas.Evaluate("N(" + p.get(i, 0) + "*" + s.impact + ","+precision+")");
				risks.add(risk);
			}
		print_risk(model.filename, risks);
				
		emPreprocessed = new DefaultDirectedWeightedGraph<Vertex, Edge>(edgeFactory);
		if (traversed_nodes.size() >= n) {
			for (int i = 0; i < n; i++) {
				Vertex s = traversed_nodes.get(i);
				Vertex s_p = addVertex(emPreprocessed, s.stateName, s.impact);
				s_p.risk = risks.get(i);
			}
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < i; j++) {
					if (P.get(i, j) != 0) {
						String iName = traversed_nodes.get(i).stateName;
						String jName = traversed_nodes.get(j).stateName;
						for (Edge e : graph.edgeSet()) {
							if (graph.getEdgeTarget(e).stateName.equals(iName)
									&& graph.getEdgeSource(e).stateName.equals(jName)) {
								Vertex vi_p = findVertex(emPreprocessed, iName);
								Vertex vj_p = findVertex(emPreprocessed, jName);
								addEdge(emPreprocessed, vj_p, vi_p, e.name, yacas.Evaluate("N(" + P1.get(i, j) +","+precision+")"));
							}
						}
					}
				}
			}
		}
		//	System.out.println("normalise the outgoing transitions");
		for (int j = 0; j < n; j++) {
			double s = 0;
			for (int i = 0; i < n; i++) {
				s += P1.get(i, j);
			}
			if (s > 0) {
				C.set(j, 0, C.get(j, 0) / s);
				for (int i = 0; i < n; i++) {
					P1.set(i, j, P1.get(i, j) / s);
				}
			}
		}
		printMatrix("P1", P1, traversed_nodes);
		Matrix p2 = I.copy().add(-1, P1).solve(C, C.copy());
		printMatrix("p2", p2, traversed_nodes);

		emNormalised = new DefaultDirectedWeightedGraph<Vertex, Edge>(edgeFactory);
		if (traversed_nodes.size() >= n)
			for (int i = 0; i < n; i++) {
				Vertex s = traversed_nodes.get(i);
				Vertex s_p = addVertex(emNormalised, s.stateName,
						yacas.Evaluate(s.impact + "*" + p.get(i, 0) / p2.get(i, 0)));
				s_p.risk = risks.get(i);
			}

		if (traversed_nodes.size() >= n)
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < i; j++) {
					if (P1.get(i, j) != 0) {
						String iName = traversed_nodes.get(i).stateName;
						String jName = traversed_nodes.get(j).stateName;
						Vertex vi_p = findVertex(emNormalised, iName);
						Vertex vj_p = findVertex(emNormalised, jName);
						for (Edge e : emPreprocessed.edgeSet()) {
							if (emPreprocessed.getEdgeTarget(e).stateName.equals(iName)
									&& emPreprocessed.getEdgeSource(e).stateName.equals(jName)) {
								addEdge(emNormalised, vj_p, vi_p, e.name, yacas.Evaluate("N(" + P1.get(i, j) +","+precision+")"));
							}
						}
					}
				}
			}
	}

	/**
	 * Check whether the matrix is low-triangular: all numbers on or above
	 * diagonal are zeroes.
	 * 
	 * @param m
	 * @return
	 */
	public static boolean isLowerTriangular(Matrix m) {
		for (int i = 0; i < m.numRows(); i++)
			for (int j = i + 1; j < m.numColumns() - 1; j++)
				if (m.get(i, j) != 0) {
					return false;
				}
		return true;
	}

	/**
	 * Computing risks by simulating a stochastic decision making process.
	 * 
	 * @graph
	 * @P - maximal number of traces
	 * @L - maximal length of a trace
	 * 
	 */
	public static void simulateRisks(Model m, int P, int L) {
		DefaultDirectedWeightedGraph<Vertex, Edge> graph = m.graph;
		String initial_state = m.initial_state;
		String final_state = m.final_state;
		System.out.println("***** simulation of initial graph (" + P + ", " + L + ") *****");
		int c = 0;
		int d = 0; // number of decisions
		HashMap<String, String> risks = new HashMap<String, String>();
		boolean debug = false;
		while (c < P) {
			if (debug)
				System.out.print(initial_state);
			Vertex previousState = findVertex(graph, initial_state);
			if (previousState != null && risks.get(initial_state) != null)
				risks.put(initial_state, yacas.Evaluate(risks.get(initial_state) + "+" + previousState.impact));
			else if (previousState != null)
				risks.put(initial_state, previousState.impact);
			String p = "1";
			for (int l = 1; l < L; l++) {
				String q = "0";
				TreeSet<Edge> es = new TreeSet<Edge>(graph.edgeSet()); // ordered
				for (Edge e : es) {
					if (graph.getEdgeSource(e) == previousState) {
						q = yacas.Evaluate(q + "+" + e.weight);
					}
				}
				String r = yacas.Evaluate("" + Math.random() + "*" + q);
				d++;
				Vertex nextState = null;
				for (Edge e : es) {
					if (graph.getEdgeSource(e) == previousState) {
						r = yacas.Evaluate(r + "-" + e.weight);
						nextState = graph.getEdgeTarget(e);
						//	System.out.println(r + "< 0");
						if (yacas.Evaluate(r + "< 0").equals("True")) {
							p = yacas.Evaluate(p + "*" + e.weight);
							break;
						}
					}
				}
				if (nextState != null) {
					if (risks.get(nextState.stateName) != null)
						risks.put(nextState.stateName, 
								yacas.Evaluate("N("+risks.get(nextState.stateName) + "+" + nextState.impact+","+precision+")"));
					else
						risks.put(nextState.stateName, yacas.Evaluate("N(" + nextState.impact + ","+precision+")"));
				}
				if (nextState == null || nextState.stateName.equals(final_state) || p.equals("0"))
					break;
				previousState = nextState;
				if (debug)
					System.out.print("=>" + previousState.stateName);
			}
			if (debug)
				System.out.println(": " + p);
			c++;
		}
		System.out.println("number of decisions = " + d);
		String risk = "0";
		for (String key : risks.keySet()) {
			Vertex v = findVertex(graph, key);
			v.risk = yacas.Evaluate("N(" + risks.get(key) + "/" + P + ","+precision+")");
			risk = risk + "+" + v.risk; 
		}
		System.out.println("Simulated total risk is " + yacas.Evaluate("N("+risk+"," + precision +")"));
		risks.clear();
	}

	/**
	 * Computing the cyclic graph's risks of a graph
	 * 
	 * @param graph
	 * @param initial_state
	 * @param final_state
	 * @throws Exception 
	 */
	public static void simulateAnalyseCyclicRisks(Model model, boolean numeric, boolean simulate) throws Exception {
		if (model == null)
			return;
		emInitial = model.graph;
		if (!simulate)
			solveRisks(model, numeric);
		else
			simulateRisks(model, 1000, 1000);
	}

	public static YacasInterpreter yacas = null;
	public static boolean numeric;
	public static boolean simulate = false;

	public static void main(String[] args) throws Exception {
		yacas = new YacasInterpreter();
		int n = 0;
		if (args.length > 0) {
			if (args[n].equals("-n")) {
				numeric = true;
				n++;
			} else
				numeric = false;
			if (args[n].equals("-g")) {
				debug = true;
				precision="3";
				n++;
			} else {
				debug = false;
				precision="5";
			}
			if (args[n].equals("-s")) {
				simulate = true;
				n++;
			} else {
				simulate = false;
			}
			int N = args.length - n;
			if (N == 1) {
				simulateAnalyseCyclicRisks(initialiseGraph_prism(args[n]), numeric, simulate);
			} if (N > 1) {
				// Parallel composition of two DTMC modules according to David Parker
				// http://www.prismmodelchecker.org/manual/ThePRISMLanguage/ParallelComposition
				String [] Ps = new String[N];
				List<List<Vertex>> Ss = new ArrayList<List<Vertex>>();
				// debug = true;
				int n_s = 1;
				for (int i = 0; i < N; i++) {
					simulateAnalyseCyclicRisks(initialiseGraph_prism(args[n]), numeric, simulate);
					Ps[i] = transition_matrix;
					List<Vertex> list = new ArrayList<Vertex>();
					list.addAll(the_traversed_nodes);
					Ss.add(list);					
					printMatrix("P["+i+"]", Ps[i], list);
					n_s *= list.size();
					n++;
				}
				String P[][] = new String[n_s][n_s];
				List<Vertex> S = new ArrayList<Vertex>();
				for (int i_s = 0; i_s<n_s; i_s++) {
					int residue = i_s;
					String impact = ")";
					String idx = ")";
					for (int i = 1; i<N; i++) {
						int n_i = Ss.get(i).size();
						if (idx == ")") {
							idx = Ss.get(i).get(residue % n_i).stateName + idx;
							impact = Ss.get(i).get(residue % n_i).impact + impact;
						} else {
							idx = Ss.get(i).get(residue % n_i).stateName + "," + idx;
							impact = Ss.get(i).get(residue % n_i).impact + "+" + impact;
						}
						residue = residue / n_i;
					}
					// System.out.println(residue);
					idx = "(" + Ss.get(0).get(residue).stateName + "," + idx;
					impact = "(" + Ss.get(0).get(residue).impact + "+" + impact;
					S.add(new Vertex(idx, impact));
				}
				for (int I = 0; I < n_s ; I++) {
					for (int J = 0; J < n_s ; J++) {
						String expr = "";
						int S_idx[] = new int[N];
						int T_idx[] = new int[N];
						int r_I = I;
						int r_J = J;
						for (int i = N-1; i>=1; i--) {
							int n_i = Ss.get(i).size();
							S_idx[i] = r_I % n_i;
							T_idx[i] = r_J % n_i;
							r_I = r_I / n_i;
							r_J = r_J / n_i;
						}
						S_idx[0] = r_I;
						T_idx[0] = r_J;
						int cnt = 0;
						for (int i=0; i<N; i++)
							if (S_idx[i]!=T_idx[i]) {
								cnt++;
							}
						if (cnt > 1) {
							P[J][I] = "0";
							continue;
						}
						// System.out.println(S.get(I).stateName + "=>" + S.get(J).stateName);
						int m = 0;
						for (int i = 0; i<N; i++) {
							// System.out.println(Ps[i]);
							String e = "(" + yacas.Evaluate(Ps[i] + "[" + (T_idx[i]+1) + "]" + "[" + (S_idx[i]+1) + "]") + ")";
							if (("" + yacas.Evaluate(e +"= 0")).equals("False")) {
								if (cnt == 0 || S_idx[i]!=T_idx[i]) {
									// System.out.println(e);
									if (expr.equals(""))
										expr = e;
									else
										expr = e + "+" + expr;
								}
								m++;
							}							
						}
						if (expr.equals("")) {
							P[J][I] = "0";
						} else {
							expr = "(" + expr + ") / " + m;
							P[J][I] = yacas.Evaluate(expr);
						}
					}
				}
				printMatrix("PQ", toMatrix(P), S);
				DefaultDirectedWeightedGraph<Vertex, Edge> graph = new DefaultDirectedWeightedGraph<Vertex, Edge>(edgeFactory); 
				for (int I = 0; I< n_s; I++) {
					graph.addVertex(new Vertex(S.get(I).stateName, S.get(I).impact));
				}
				for (int I = 0; I < n_s ; I++)
					for (int J = 0; J < n_s ; J++) {
						if (("" + yacas.Evaluate(P[J][I] +"= 0")).equals("False"))
						{
							Vertex vs = findVertex(graph, S.get(I).stateName);
							Vertex vt = findVertex(graph, S.get(J).stateName);
							graph.addEdge(vs, vt);
						}
					}
				Model m = new Model();
				m.graph = graph;
				m.filename = args[n-1].substring(0, args[n-1].lastIndexOf("/")) + "/" + "composed";
				symbolicSolve(m, P, S);				
			}
		}
	}

	static class Model {
		String filename;
		String initial_state;
		String final_state;
		DefaultDirectedWeightedGraph<Vertex, Edge> graph;
	}

	private static Model initialiseGraph_prism(String model) throws FileNotFoundException {
		Model m = new Model();
		m.graph = new DefaultDirectedWeightedGraph<Vertex, Edge>(edgeFactory);
		m.filename = model;
		File pm = new File(model + ".pm"); // prism model
		File lab = new File(model + ".lab"); // labels
		File tra = new File(model + ".tra"); // transition matrix
		File sta = new File(model + ".sta"); // states
		File srew = new File(model + ".srew"); // state rewards => impact
		if (! pm.exists()) { 
			System.err.println("Prism model input file" + model + ".pm is not found!");
			return null;
		}
		if (! lab.exists() || !tra.exists() || !sta.exists() || !srew.exists()) {
			String cmd = "./prism/prism/bin/prism " + model + ".pm -exportmodel " + model + ".all -const p=0.5 -exportss " + model + ".ss -ss";
			System.err.println("Prism output file not found, please run the following command:\n" + cmd);
			return null;
		}
		
		// parse states
		Scanner s = new Scanner(sta);
		String first_line = s.nextLine();
		String variables[] = first_line.substring(1, first_line.lastIndexOf(")")).split(" ");
		while (s.hasNextLine()) {
			String line = s.nextLine();
			String pair[] = line.split(":");
			String key = pair[0];
			String value = pair[1].substring(1,  pair[1].lastIndexOf(")"));
			String values[] = value.split(" ");
			Vertex v = addVertex(m.graph, key, "0");
			for (int i=0; i<variables.length; i++)
				v.stateValues = variables[i] + "=" + values[i] + "\n";
		}
		s.close();
		
		// parse state rewards as impact
		s = new Scanner(srew);
		first_line = s.nextLine();
		String args[] = first_line.split(" ");
		int n = Integer.parseInt(args[0]); // number of states
		assert(n == m.graph.vertexSet().size());		
		int size = Integer.parseInt(args[1]); // number of non-zero impacts
		for (int i = 0; i < size; i++) {
			if (s.hasNextLine()) {
				String line = s.nextLine();
				String pair[] = line.split(" ");
				Vertex state = findVertex(m.graph, pair[0]);
				state.impact = pair[1];
			}			
		}
		s.close();
		
		// parse the state labels
		s = new Scanner(lab);
		first_line = s.nextLine();
		String values[] = first_line.split(" ");
		Map<String, String> state_types = new HashMap<String, String>();
		for (int i=0; i<values.length; i++) {
			String pair[] = values[i].split("=");
			state_types.put(pair[0], pair[1]);
		}
		while (s.hasNextLine()) {
			String line = s.nextLine();
			String pair[] = line.split(": ");
			if (state_types.get(pair[1]).equals("\"init\"")) {
				m.initial_state = pair[0];
			}
			if (state_types.get(pair[1]).equals("\"deadlock\"")) {
				m.final_state = pair[0];
			}
		}
		s.close();
		
		// parse the transitions
		s = new Scanner(tra);
		first_line = s.nextLine();
		values = first_line.split(" ");		
		n = Integer.parseInt(values[0]); // number of states
		assert(n == m.graph.vertexSet().size());		
		size = Integer.parseInt(values[1]);
		for (int i = 0; i < size; i++) {
			if (s.hasNextLine()) {
				String line = s.nextLine();
				String triple[] = line.split(" ");
				Vertex s0 = findVertex(m.graph, triple[0]);
				Vertex s1 = findVertex(m.graph, triple[1]);
				if (m.final_state == null || !m.final_state.equals(triple[0]))
					addEdge(m.graph, s0, s1, "", triple[2]); // TODO label of the transition
			}
			
		}
		s.close();
		
		// printGraph(m.graph);
		return m;
	}

	static EdgeFactory<Vertex, Edge> edgeFactory = new ClassBasedEdgeFactory<Vertex, Edge>(Edge.class);

}
