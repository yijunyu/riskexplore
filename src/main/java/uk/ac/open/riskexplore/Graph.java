package uk.ac.open.riskexplore;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.geom.Rectangle2D;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

import org.jgraph.JGraph;
import org.jgraph.graph.AttributeMap;
import org.jgraph.graph.GraphConstants;
import org.jgrapht.ext.JGraphModelAdapter;
import org.jgrapht.graph.DefaultDirectedWeightedGraph;

import com.jgraph.components.labels.CellConstants;
import com.jgraph.components.labels.MultiLineVertexRenderer;
import com.jgraph.layout.JGraphFacade;
import com.jgraph.layout.hierarchical.JGraphHierarchicalLayout;

/**
 * A demo applet that shows how to use JGraph to visualize JGraphT graphs.
 *
 * @author Barak Naveh
 *
 * @since Aug 3, 2003
 */
public class Graph extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Color DEFAULT_BG_COLOR = Color.decode("#FAFBFF");
	private static final Dimension DEFAULT_SIZE = new Dimension(600, 600);

	class ExtJGraphModelAdapter extends JGraphModelAdapter<Vertex, Edge> {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public ExtJGraphModelAdapter(org.jgrapht.Graph<Vertex, Edge> jGraphTGraph) {
			super(jGraphTGraph);
		}

		@Override
		public AttributeMap getDefaultEdgeAttributes() {
			AttributeMap map = new AttributeMap();
			GraphConstants.setLineEnd(map, GraphConstants.ARROW_TECHNICAL);
			GraphConstants.setEndFill(map, true);
			GraphConstants.setLabelAlongEdge(map, true);
			GraphConstants.setForeground(map, Color.decode("#25507C"));
			// GraphConstants.setFont(map,
			GraphConstants.DEFAULTFONT.deriveFont(Font.BOLD, 6);
			GraphConstants.setLineColor(map, Color.decode("#7AA1E6"));
			return map;
		}

		@Override
		public AttributeMap getDefaultVertexAttributes() {
			AttributeMap map = new AttributeMap();
			// we set the bounds only to force the layout manager to use minimum
			// size shapes
			// this way the auto sizing mechanism will immediately generate the
			// optimum sized shapes
			GraphConstants.setBounds(map, new Rectangle2D.Double(0, 0, 50, 50));
			GraphConstants.setAutoSize(map, true);
			GraphConstants.setBackground(map, Color.LIGHT_GRAY);
			GraphConstants.setForeground(map, Color.BLACK);
			GraphConstants.setOpaque(map, true);
			GraphConstants.setFont(map, GraphConstants.DEFAULTFONT.deriveFont(Font.BOLD, 12));
			GraphConstants.setBorderColor(map, Color.black);
			// we want to have the nodes represented as circles
			// but, the auto size property overrides this and
			// ellipses are created
			CellConstants.setVertexShape(map, MultiLineVertexRenderer.SHAPE_ROUNDED);
			return map;
		}
	};

	public Graph(DefaultDirectedWeightedGraph<Vertex, Edge> graph) {
		ExtJGraphModelAdapter m_jgAdapter = new ExtJGraphModelAdapter(graph);
		JGraph jgraph = new JGraph(m_jgAdapter);
		adjustDisplaySettings(jgraph);
		JGraphFacade jgf = new JGraphFacade(jgraph);
		jgf.setDirected(true);
//		jgf.setEdgePromotion(true);
		// JGraphLayout jgl = new JGraphFastOrganicLayout();
		// JGraphLayout jgl = new JGraphOrganicLayout();
		// JGraphLayout jgl = new JGraphSelfOrganizingOrganicLayout();
		// JGraphLayout jgl = new JGraphRadialTreeLayout();
//		JGraphTreeLayout jgl = new JGraphTreeLayout();
		JGraphHierarchicalLayout jgl = new JGraphHierarchicalLayout();
		jgl.setOrientation(1);
		jgl.run(jgf);
		Map<?, ?> nested = jgf.createNestedMap(true, true);
		jgraph.getGraphLayoutCache().edit(nested);
		getContentPane().add(new JScrollPane(jgraph));
		setLocationByPlatform(true);
		if (graph.vertexSet().size() < 8)
			jgraph.setScale(1.5);
		else
			jgraph.setScale(0.8);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void adjustDisplaySettings(JGraph jg) {
		jg.setPreferredSize(DEFAULT_SIZE);
		Color c = DEFAULT_BG_COLOR;
		jg.setBackground(c);
	}

	public static void main(String args[]) throws Exception {
		Search.displayMode = Search.DisplayMode.PLAIN;
		Search.main(args);
		Search.displayMode = Search.DisplayMode.HTML;
		Graph graph = new Graph(Search.emInitial);
		graph.setTitle("Exploration machine before preprocessing");
		graph.pack();
		graph.setVisible(true);
		Graph graph2 = new Graph(Search.emPreprocessed);
		graph2.setTitle("Exploration machine after preprocessing");
		graph2.pack();
		graph2.setVisible(true);
		Graph graph3 = new Graph(Search.emNormalised);
		graph3.setTitle("Exploration machine after normalisation");
		graph3.pack();
		graph3.setVisible(true);
	}
}
