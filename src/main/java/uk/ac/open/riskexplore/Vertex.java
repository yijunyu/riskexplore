package uk.ac.open.riskexplore;

public class Vertex {
	public String stateName;
	public String impact;
	public String risk; // record the result of risk computation
	public String stateValues;
	public Vertex(String name, String impact) {
		stateName = name;
		this.impact = impact;
	}
	
	public String toString() {
		if (risk==null)
			risk = "0";
		if (Search.displayMode == Search.DisplayMode.HTML)
			return "<html>&nbsp;&nbsp;&nbsp;" + stateName + "&nbsp;&nbsp;&nbsp;<br/><center><font size='3' color='green'>i: " + impact + "</font>  <font size='3' color='red'>r: "
				+risk+"</font></center></html>";		
		return stateName + " (" + impact + ")";
	}
	
	@Override
	public boolean equals(Object v) {
		if (v instanceof Vertex)
			return this.stateName.equals(((Vertex) v).stateName);
		return false;
	}

	@Override
	public int hashCode() {
	    int hash = stateName.hashCode();
	    return hash;
	}
	
}
